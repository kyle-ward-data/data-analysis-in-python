'''
Script for creating eventlog.csv from incidents.csv and system_audit.csv
'''

__author__ = "Kyle Ward"

import os
import pandas as pd
import numpy as np

#-----------------------------------------------------------------------
# UTILS

def clean_fieldname(**kwargs):
    field = kwargs['field'].replace('_', ' ')
    return field.title()

def numeric_change_tranformation(**kwargs):
    field = clean_fieldname(field=kwargs['field'])
    if kwargs['oldvalue'] is None:
        ov = 0
    else:
        ov = kwargs['oldvalue']

    if float(ov) > float(kwargs['newvalue']):
        return f'{field} - Decrease'
    else:
        return f'{field} - Increase'

def null_or_key_transformation(**kwargs):
    field = clean_fieldname(field=kwargs['field'])
    if kwargs['oldvalue'] is not None:
        return f'{field} - Updated'
    else:
        return f'{field} - Initial/Update from Empty'

def transform_log(field, oldvalue, newvalue):
    if field in TRANSFORM_MAPPER.keys():
        operator = TRANSFORM_MAPPER[field]
        return operator(field=field, oldvalue=oldvalue, newvalue=newvalue)
    else:
        return field


#-----------------------------------------------------------------------
# SETTINGS

TRANSFORM_MAPPER = {
    'escalation': numeric_change_tranformation,
    'assigned_to': null_or_key_transformation,
    'severity': numeric_change_tranformation,
    'u_category':null_or_key_transformation,
    'incident_state': numeric_change_tranformation,
    'priority': numeric_change_tranformation,
    'urgency': numeric_change_tranformation,
    'company':null_or_key_transformation,
    'impact': numeric_change_tranformation,
    'location': null_or_key_transformation,
    'made_sla':clean_fieldname,
    'comments':clean_fieldname
}


#-----------------------------------------------------------------------
# ETL FUNCTIONS

def create_incident_eventlog(df: pd.DataFrame):
    '''Format, clean, and export incident csv for process mining.'''

    # convert nan to None
    df = df.where(pd.notnull(df), None)

    # filter to only closed incidents
    df = df[df['closed_at'].notna()].copy()

    # melt to get open/closed timestamps for each row
    df = pd.melt(df, id_vars=['sys_id'], value_vars=['opened_at', 'closed_at'])

    # rename headers
    rename_headers_as = {
            'sys_id': 'case_id',
            'variable':'activity_name',
            'value':'timestamp'
        }
    df.rename(columns=rename_headers_as, inplace=True)

    # update names for open/close
    df['activity_name'] = df['activity_name'].replace('opened_at', 
                                                    'Incident Opened')
    df['activity_name'] = df['activity_name'].replace('closed_at', 
                                                    'Incident Closed')
    
    return df
    

def create_system_audit_eventlog(df: pd.DataFrame, 
                                relevent_cases: pd.Series = None):
    '''Format, clean, and export system_audit csv for process mining.'''

     # convert nan to None
    df = df.where(pd.notnull(df), None)

    # run logic to assign activity_name
    df.loc[:,'activity_name'] = df.apply(lambda x: 
                                            transform_log(x['fieldname'], 
                                                        x['oldvalue'], 
                                                        x['newvalue']), 
                                            axis=1
                                        )
    rename_headers_as = {
            'document_key': 'case_id',
            'sys_created_on':'timestamp'
        }

    df.rename(columns=rename_headers_as, inplace=True)

    if relevent_cases is not None:
        df = df[df['case_id'].isin(relevent_cases)].copy()
    
    df = df[['case_id','timestamp','activity_name']].copy()

    return df

#-----------------------------------------------------------------------
# MAIN SCRIPT

if __name__ == '__main__':

    # data folder
    DATA_PATH = R'./servicenow_datasets'
    
    # input paths
    INCIDENT_PATH = os.path.join(DATA_PATH, 'incident.csv')
    SYSTEM_AUDIT_PATH = os.path.join(DATA_PATH, 'system_audit.csv')
    
    # output path
    EVENTLOG_PATH = os.path.join(DATA_PATH, 'eventlog.csv')

    # load data
    df_in = pd.read_csv(INCIDENT_PATH)
    df_sa = pd.read_csv(SYSTEM_AUDIT_PATH)
    
    # create eventlog.csv
    df_el_in = create_incident_eventlog(df_in)
    df_el_sa = create_system_audit_eventlog(df_sa, 
                                            relevent_cases=df_el_in['case_id'])
    
    # UNION TABLES
    df_el = pd.concat([df_el_in, df_el_sa], axis=0, ignore_index=True)
    
    # normalize timestamp
    # done twice to work, don't have time to look into why currently
    df_el.loc[:,'timestamp'] = pd.to_datetime(df_el['timestamp'])
    df_el.loc[:,'timestamp'] = pd.to_datetime(df_el['timestamp'], 
                                                format='%Y-%m-%dT%H:%M%:%SZ')

    # output eventlog
    df_el.to_csv(EVENTLOG_PATH, index=False)
