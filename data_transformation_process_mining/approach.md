<span style="font-size: 32px;"><b>Project & Code Review:</b> Data Transformations for Process Mining</span>

---

by **Kyle Ward** on *Tuesday, May 17, 2022*

**Goal:** After reading this document, readers should understand the problem, the approach, and how it was solved.

<br>

<span style="font-size: 24px">Contents</span>

---

- [What is Process Mining?](#what-is-process-mining)
- [Prompt](#prompt)
- [Data Transformations](#data-transformations)
  - [Incident](#incident)
  - [System Audit](#system-audit)
  - [Eventlog](#eventlog)
- [Brainstorming Solutions](#brainstorming-solutions)
- [Programming a Solution in Python](#programming-a-solution-in-python)
  - [Main Script](#main-script)
  - [ETL Functions](#etl-functions)
  - [Utils and Settings](#utils-and-settings)



<div class="page"/>


# What is Process Mining?

This free e-book has a great introduction on what process mining is and why it's important:

[https://fluxicon.com/book/read/intro/](https://fluxicon.com/book/read/intro/)



<div class="page"/>


# Prompt

You are an senior analyst at a consulting firm. Your client, Clueless Inc., is having trouble analyzing their IT ticketing process. They have come to you to help them solve this problem. With your knowledge of application systems and process mining, you ask them to provide the log files for their IT ticketing application. They provide you with two files:

- `incident.csv`
- `system_audit.csv`

You know that to fully analyze the process, you're going to have to clean and transform the data into 1 file.

That file should be called `eventlog.csv` and have the following schema:

| Field         | Description                                                 |
| ------------- | ----------------------------------------------------------- |
| case_id       | Unique ID for the specific "ticket case"                    |
| activity_name | Activity performed around that case                         |
| timestamp     | The date and time the event was recorded in the application |

You'll use this file with a 3rd party process mining application to analyze and deliver the results.

<div class="page"/>

# Data Transformations

## Incident

For the incident file we need to transform this:


| sys_id                           | ... | opened_at                     | closed_at                     | ... |
| -------------------------------- | --- | ----------------------------- | ----------------------------- | --- |
| 5b819baa1babbfc0963ddc65bd4bcbf2 | ... | 2019-09-01T00:07:25.000+00:00 | 2019-09-01T00:24:29.000+00:00 | ... |
|                                  |     |                               |                               |     |

To this:

| case_id                          | activity_name   | timestamp                     |
| -------------------------------- | --------------- | ----------------------------- |
| 5b819baa1babbfc0963ddc65bd4bcbf2 | Incident Opened | 2019-09-01T00:07:25.000+00:00 |
| 5b819baa1babbfc0963ddc65bd4bcbf2 | Incident Closed | 2019-09-01T00:24:29.000+00:00 |
|                                  |                 |                               |

## System Audit

For the System Audit file we need to transform this:

| document_key                     | sys_created_on       | oldvalue | newvalue                         | fieldname |
| -------------------------------- | -------------------- | -------- | -------------------------------- | --------- |
| 54b2a7071be7bb0476543150cd4bcbd8 | 2019-09-06T08:18:56Z | 3        | 2                                | urgency   |
| eed958e31b6f774476543150cd4bcb43 | 2019-09-03T13:26:30Z |          | 025b9af46fde61004e56199fae3ee417 | company   |
|                                  |                      |          |                                  |           |

To this:

| case_id                          | activity_name                       | timestamp            |
| -------------------------------- | ----------------------------------- | -------------------- |
| 54b2a7071be7bb0476543150cd4bcbd8 | Urgency - Decrease                  | 2019-09-06T08:18:56Z |
| eed958e31b6f774476543150cd4bcb43 | Company - Initial/Update from Empty | 2019-09-03T13:26:30Z |
|                                  |                                     |                      |

## Eventlog

| case_id                          | activity_name                       | timestamp                     |
| -------------------------------- | ----------------------------------- | ----------------------------- |
| 5b819baa1babbfc0963ddc65bd4bcbf2 | Incident Opened                     | 2019-09-01T00:07:25.000+00:00 |
| 5b819baa1babbfc0963ddc65bd4bcbf2 | Incident Closed                     | 2019-09-01T00:24:29.000+00:00 |
| 54b2a7071be7bb0476543150cd4bcbd8 | Urgency - Decrease                  | 2019-09-06T08:18:56Z          |
| eed958e31b6f774476543150cd4bcb43 | Company - Initial/Update from Empty | 2019-09-03T13:26:30Z          |
|                                  |                                     |                               |


<div class="page"/>

# Brainstorming Solutions

There are usually many, often too many, methods/approaches to solve a problem.

Here are some thoughts on achieving this task:

- Spreadsheets: Do this task manually in a spreadsheet software of your choice
  - Pros:
    - Low skill barrier to complete, can delegate to other analysts or interns
    - Most people you work with can understand/transfer the work
  - Cons:
    - Time-consuming
    - Painfully tedious work, even when using best practices/shortcuts
    - Difficult to automate/repeat without at least doing some busy work
    - Prone to error
- Workflow data transformation tools (Alteryx, etc)
  - Pros:
    - Easy to drag and drop solution
    - Repeatable with options to expand
  - Cons:
    - Skill barrier to learning software
    - Confined to software features
    - Can be expensive
- Programming: Write a script in the language of your choice
  - Pros:
    - Repeatable process
    - Can transfer into production environments
    - Can automate easily
    - Full control of approach
  - Cons
    - Higher barrier to entry, must learn programming language
    - Coworkers may not be able to collaborate


<div class="page"/>

# Programming a Solution in Python

There are a lot of different ways to solve this problem in python, the solution presented here is just one example.


For this solution, I broke the code up into 4 components:

- Main Script
- ETL Functions
- Settings
- Utils

Note: I put all the code into one file since it was only ~150 lines of code. However, on a larger scale project I would have broken the components out into their own files or directories.

In the following sections, I will breakdown the code in the order that it is run, to clearly understand what is happening.





<div class="page"/>

## Main Script

---

```python
if __name__ == '__main__':

    # data folder
    DATA_PATH = R'./servicenow_datasets'
    
    # input paths
    INCIDENT_PATH = os.path.join(DATA_PATH, 'incident.csv')
    SYSTEM_AUDIT_PATH = os.path.join(DATA_PATH, 'system_audit.csv')
    
    # output path
    EVENTLOG_PATH = os.path.join(DATA_PATH, 'eventlog.csv')

    # load data
    df_in = pd.read_csv(INCIDENT_PATH)
    df_sa = pd.read_csv(SYSTEM_AUDIT_PATH)
    
    # create eventlog.csv
    df_el_in = create_incident_eventlog(df_in)
    df_el_sa = create_system_audit_eventlog(df_sa, 
                                            relevent_cases=df_el_in['case_id'])
    
    # UNION TABLES
    df_el = pd.concat([df_el_in, df_el_sa], axis=0, ignore_index=True)
    
    # normalize timestamp
    # done twice to work, don't have time to look into why currently
    df_el.loc[:,'timestamp'] = pd.to_datetime(df_el['timestamp'])
    df_el.loc[:,'timestamp'] = pd.to_datetime(df_el['timestamp'], 
                                                format='%Y-%m-%dT%H:%M%:%SZ')

    # output eventlog
    df_el.to_csv(EVENTLOG_PATH, index=False)
```

This code is the core or "main" script to run the data transformation process. The point of this code is only to execute the logic of the transformations, although some of the data work is also included here. It is fairly self explanatory, as I try to make my code as readable as possible. I add a lot of comments, but I could probably remove some if I wanted.

Process:
- Load data files
- Transform them into eventlog format
- Union/concat them together
- Do some last minute formatting
- Export the eventlog data


<div class="page"/>

## ETL Functions

**Incident**

```python
def create_incident_eventlog(df: pd.DataFrame):
    '''Format, clean, and export incident csv for process mining.'''

    # convert nan to None
    df = df.where(pd.notnull(df), None)

    # filter to only closed incidents
    df = df[df['closed_at'].notna()].copy()

    # melt to get open/closed timestamps for each row
    df = pd.melt(df, id_vars=['sys_id'], value_vars=['opened_at', 'closed_at'])

    # rename headers
    rename_headers_as = {
            'sys_id': 'case_id',
            'variable':'activity_name',
            'value':'timestamp'
        }
    df.rename(columns=rename_headers_as, inplace=True)

    # update names for open/close
    df['activity_name'] = df['activity_name'].replace('opened_at', 
                                                    'Incident Opened')
    df['activity_name'] = df['activity_name'].replace('closed_at', 
                                                    'Incident Closed')
    
    return df
```

The `incident` transformation is much easier than the `system_audit`.

Process:
- Fix null values
- Filter to only closed incidents
- `.melt` the data into the correct format
- Rename the columns to the correct format
- Rename the fields to be more reader friendly
- Return data

<div class="page"/>

**System Audit**

```python
def create_system_audit_eventlog(df: pd.DataFrame, 
                                relevent_cases: pd.Series = None):
    '''Format, clean, and export system_audit csv for process mining.'''

     # convert nan to None
    df = df.where(pd.notnull(df), None)

    # run logic to assign activity_name
    df.loc[:,'activity_name'] = df.apply(lambda x: 
                                            transform_log(x['fieldname'], 
                                                        x['oldvalue'], 
                                                        x['newvalue']), 
                                            axis=1
                                        )
    rename_headers_as = {
            'document_key': 'case_id',
            'sys_created_on':'timestamp'
        }

    df.rename(columns=rename_headers_as, inplace=True)

    if relevent_cases is not None:
        df = df[df['case_id'].isin(relevent_cases)].copy()
    
    df = df[['case_id','timestamp','activity_name']].copy()

    return df
```

This process is seemingly simple, however, the `transform_log` is doing a lot of work and will be covered in detail during the `Settings`/`Utils` sections next. That said, all it does is use the `fieldname` to match the correct transformation function to apply to the `oldvalue` and the `newvalue`.

Process:
- Fix null values
- Apply the `transform_log` function to all the rows to create the `activity_name` column
- Rename the columns to the correct format
- Filter to the relevant cases (closed incidents)
- Format to only needed columns
- Return Data



<div class="page"/>

## Utils and Settings

```python
def numeric_change_tranformation(**kwargs):
    field = clean_fieldname(field=kwargs['field'])
    if kwargs['oldvalue'] is None:
        ov = 0
    else:
        ov = kwargs['oldvalue']

    if float(ov) > float(kwargs['newvalue']):
        return f'{field} - Decrease'
    else:
        return f'{field} - Increase'

def null_or_key_transformation(**kwargs):
    field = clean_fieldname(field=kwargs['field'])
    if kwargs['oldvalue'] is not None:
        return f'{field} - Updated'
    else:
        return f'{field} - Initial/Update from Empty'

def transform_log(field, oldvalue, newvalue):
    if field in TRANSFORM_MAPPER.keys():
        operator = TRANSFORM_MAPPER[field]
        return operator(field=field, oldvalue=oldvalue, newvalue=newvalue)
    else:
        return field

#-----------------------------------------------------------------------
# SETTINGS
TRANSFORM_MAPPER = {
    'escalation': numeric_change_tranformation,
    'assigned_to': null_or_key_transformation,
    'severity': numeric_change_tranformation,
    'u_category':null_or_key_transformation,
    'incident_state': numeric_change_tranformation,
    'priority': numeric_change_tranformation,
    'urgency': numeric_change_tranformation,
    'company':null_or_key_transformation,
    'impact': numeric_change_tranformation,
    'location': null_or_key_transformation,
    'made_sla':clean_fieldname,
    'comments':clean_fieldname
}
```

The key code here is the `transform_log` function. This data returned is what goes into each cell for the `activity_name` column.

`transform_log` Process:
- Use `fieldname` to lookup function to use in the settings `TRANSFORM_MAPPER`
- Pass all variables to the `operator` function
- The data is processed and returns the output of the operator