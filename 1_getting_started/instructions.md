<span style="font-size: 32px;"><b>Data Analysis in Python:</b> Getting Started</span>

---

by **Kyle Ward** on *Monday, April 18, 2022*

**Goal:** After completing this tutorial, readers should understand some basics of data analysis with python, setting up an environment, and how to perform some simple data tasks.

<br>

<span style="font-size: 24px">Contents</span>

---

- [What is Data Analysis?](#what-is-data-analysis)
  - [Definition](#definition)
  - [Data Analysis Process Frameworks](#data-analysis-process-frameworks)
  - [Data Analysis Tools](#data-analysis-tools)
- [What is Python?](#what-is-python)
  - [Definition](#definition-1)
  - [Where is Python used?](#where-is-python-used)
  - [Average Salary with Python Skills](#average-salary-with-python-skills)
- [Setting up Python as a Data Analysis Tool](#setting-up-python-as-a-data-analysis-tool)
  - [Overview](#overview)
  - [Installing Python](#installing-python)
    - [Option A: Python.org](#option-a-pythonorg)
    - [Option B: Anaconda Distribution](#option-b-anaconda-distribution)
  - [OPTIONAL: Setting Up a Virtual Environment](#optional-setting-up-a-virtual-environment)
  - [Installing Packages/Libraries: Pandas, NumPy, and more](#installing-packageslibraries-pandas-numpy-and-more)
    - [Option A: Python pip](#option-a-python-pip)
    - [Option B: Anaconda Distribution](#option-b-anaconda-distribution-1)
  - [Installing Text Editor: JupyterLab](#installing-text-editor-jupyterlab)
    - [Option A: pip](#option-a-pip)
    - [Option B: Anaconda Distribution](#option-b-anaconda-distribution-2)
- [Create a Jupyter File for the Analysis](#create-a-jupyter-file-for-the-analysis)
  - [Launch JupyterLab or JupyterNotebook and Create Notebook File](#launch-jupyterlab-or-jupyternotebook-and-create-notebook-file)
  - [Understand Jupyter File "Cell" Options](#understand-jupyter-file-cell-options)
- [Data Analysis Workshop](#data-analysis-workshop)
  - [Prompt](#prompt)
  - [Tasks](#tasks)
    - [Import the Packages/Libraries](#import-the-packageslibraries)
    - [Load the Data](#load-the-data)
    - [Describe the Data](#describe-the-data)
    - [Describe the Data with Pandas Profiling](#describe-the-data-with-pandas-profiling)
    - [Answer the Prompt Questions](#answer-the-prompt-questions)



<div class="page"/>


# What is Data Analysis?

## Definition



> "Data analysis is a process of inspecting, cleansing, transforming, and modeling data with the goal of discovering useful information, informing conclusions, and supporting decision-making."
>
> -The Free Dictionary (https://encyclopedia.thefreedictionary.com/Data+analysis)

<!-- %%{init: {'theme': 'base', 'themeVariables': { 'primaryColor': '71B5CC', 'secondaryColor': '#D99C78'}}}%% -->

<br>

```mermaid
%%{init: {'theme': 'base', 'themeVariables': { 'primaryColor': '#c9dfed' }}}%%
stateDiagram 
direction LR
Data --> DataAnalysis

state DataAnalysis {
direction LR
  Inspecting
  Cleansing
  Transforming
  Modeling

}

DataAnalysis --> Information

```


<div class="page"/>

## Data Analysis Process Frameworks

Many data professionals use some sort of process framework, which is basically a detailed checklist, to complete a data analysis (or data mining/data science) project. Some professionals use their own or their company provides one, however, most use some variation of the **CRISP-DM** framework. CRISP-DM stands for *Cross Industry Standard Process for Data Mining*.

The diagram below gives an overview of CRISP-DM:

<img width=500px src=".md_files/crisp-dm.png">


Some other notable process frameworks are **SEMMA** (Sample, Explore, Modify, Model, Assess) and **KDD Process** (Knowledge Discovery in Databases), but they will not be covered.

<div class="page"/>

## Data Analysis Tools

>Tools should be viewed as tools, there are many and they will change over time.

There are many options in regard to data analysis tools, and new tools are being developed every year. There are spreadsheets, programming languages, industry-specific software, and even pen/paper. Each project will have different requirements and different tool needs.

However, the tool used is not that important, what's important is the information as a result of the data analysis.

<br>

```mermaid
%%{init: {'theme': 'base', 'themeVariables': { 'primaryColor': '#c9dfed'}}}%%
stateDiagram 
direction LR

Data --> DataAnalysis

state Tools {
    direction LR

    Spreadsheets
    Python
    R
    PowerBI
    ...
}

Tools --> DataAnalysis   

DataAnalysis --> Information

```

<div class="page"/>

# What is Python?

## Definition

>"Python is an interpreted, object-oriented, high-level programming language with dynamic semantics. Its high-level built in data structures, combined with dynamic typing and dynamic binding, make it very attractive for Rapid Application Development, as well as for use as a scripting or glue language to connect existing components together. Python's simple, easy to learn syntax emphasizes readability and therefore reduces the cost of program maintenance. Python supports modules and packages, which encourages program modularity and code reuse. The Python interpreter and the extensive standard library are available in source or binary form without charge for all major platforms, and can be freely distributed."
>
>-Python.org (https://www.python.org/doc/essays/blurb/)

<br>

## Where is Python used?

Python is can be used almost everywhere for almost anything, but some of its most common uses are the following:

- Web Application Development
- Data Analytics & Data Science
- Data Engineering
- Looking Cool at Coffee Shops

<br>


## Average Salary with Python Skills

According to payscale data, the average base salary for jobs that require skills in python is $93k. https://www.payscale.com/research/US/Skill=Python/Salary

<div class="page"/>

# Setting up Python as a Data Analysis Tool

## Overview

A typical python data analysis environment has several components:

1. Python language instance installed on the computer
2. Python data analysis packages/libraries installed on the python instance
3. A text editor to write the code

**There are many different ways to set this up,** but this document will only cover one way to do this. It's encouraged to read, watch, and try other ways to see what works best for each individual.

<div class="page"/>

## Installing Python

There are many ways to install Python. Only two options are outlined. 

### Option A: Python.org

This is the method I prefer and use the most. However, it may not be the most user-friendly approach for learning to program in Python.

**Check if Python 3.X is already installed.**

Mac and Linux both, typically, have Python installed. Depending on the system version, there should be two versions of Python: a 2.X and 3.X version. Windows usually doesn't have Python preinstalled but it's good to check.

Open a terminal and write the following:

*MacOS/Linux*

```bash
$ python3 --version
```

*Windows*

```bash
$ python --version
```

It's important to type `python3` and not just `python` as systems with both will use `python` for Python 2.X. Python 2.X is no longer supported and should be avoided.

It should output the version if it's installed.

```bash
Python 3.X.X
```

**If Python 3.X is not installed, install Python 3.X from Python.org**

Go to [https://www.python.org/downloads/](https://www.python.org/downloads/) and follow the directions for the relevant operating system.

<div class="page"/>

### Option B: Anaconda Distribution

>Anaconda is a distribution of the Python and R programming languages for scientific computing (data science, machine learning applications, large-scale data processing, predictive analytics, etc.), that aims to simplify package management and deployment. The distribution includes data-science packages suitable for Windows, Linux, and macOS.
> 
> [https://en.wikipedia.org/wiki/Anaconda_(Python_distribution)](https://en.wikipedia.org/wiki/Anaconda_(Python_distribution))


Go to [https://www.anaconda.com/products/distribution](https://www.anaconda.com/products/distribution) and follow the install directions for the relevant system.

<div class="page"/>

## OPTIONAL: Setting Up a Virtual Environment

If python was installed using Option A, a good practice to follow is setting up virtual python environments. If packages are installed directly to the systems Python instance, different projects might require different versions of packages and that can create conflicts that are annoying to fix. Also, if a project needs to be shared with others, it can be hard to determine which packages are needed. 

Python virtual environments are a great way to minimize package conflicts and determine which packages are needed for a project. Python virtual environments create a "virtual" instance of python, that can be activated for specific projects or uses.

Follow the documentation here [https://docs.python.org/3/library/venv.html](https://docs.python.org/3/library/venv.html) to create and activate a virtual environment.

**Open a terminal and navigate to where the environment should be created.** 

```bash
python -m venv env
```

**Activate the environment.**

*MacOS/Linux:*


```bash
source env/bin/activate
```

*Windows*

```bash
env\Scripts\activate.bat
```

**Deactivate the environment.**

```bash
deactivate
```


<div class="page"/>

## Installing Packages/Libraries: Pandas, NumPy, and more

### Option A: Python pip

>pip is a package-management system written in Python used to install and manage software packages. It connects to an online repository of public packages, called the Python Package Index. pip can also be configured to connect to other package repositories (local or remote), provided that they comply to Python Enhancement Proposal 503.
>
>https://en.wikipedia.org/wiki/Pip_(package_manager)

Pip documentation: [https://pip.pypa.io/en/stable/user_guide/](https://pip.pypa.io/en/stable/user_guide/)

```bash
pip install pandas
```

```bash
pip install numpy
```

### Option B: Anaconda Distribution

It's possible to manage packages through the Anaconda GUI application or through the `conda` package manager.

Please reference this tutorial for details: [https://docs.anaconda.com/anaconda/navigator/tutorials/manage-packages/](https://docs.anaconda.com/anaconda/navigator/tutorials/manage-packages/)

<div class="page"/>

## Installing Text Editor: JupyterLab

### Option A: pip

**Installing**

JupyterLab install documentation: [https://jupyterlab.readthedocs.io/en/stable/getting_started/installation.html](https://jupyterlab.readthedocs.io/en/stable/getting_started/installation.html)

```bash
pip install jupyterlab
```

**Launching**

Open a terminal and navigate to the desired directory, run the following command:

```bash
jupyter lab
```

It should launch the application in the terminal and open a tab in the browser. If it doesn't open, type `http://localhost:8888/lab` into the browser.

### Option B: Anaconda Distribution

Jupyter Notebooks is normally preinstalled with an instance of the Anaconda Distribution, it can be launched using the GUI.


<div class="page"/>

# Create a Jupyter File for the Analysis

## Launch JupyterLab or JupyterNotebook and Create Notebook File

Launch JupyterLab or JupyterNotebook in the project directory.

Create a new file either in the Jupyter launcher or by creating a new file with the `.ipynb` extension.

## Understand Jupyter File "Cell" Options

Each "cell" in a jupyter file can either be Python code, Markdown, or raw text.

Python code is, of course, python code to be executed. 

Markdown is defined as the following:

> Markdown is a lightweight markup language for creating formatted text using a plain-text editor.
> 
> [https://en.wikipedia.org/wiki/Markdown](https://en.wikipedia.org/wiki/Markdown)

It's great for adding nicely formatted text to a Jupyter File.

Please reference the Markdown documentation: [https://www.markdownguide.org/](https://www.markdownguide.org/)


<div class="page"/>

# Data Analysis Workshop


## Prompt

You work for a small retailer with several stores in the area. The owner would like to understand their sales data a bit more and asked you if you could analyze it. The owner pulled the data from their Point of Sale (PoS) system as a  `.csv` file and gave it you. The file is labeled `sales_transactions.csv`.

The owner also gave you a list of questions they want answered from your analysis.

1. How many sales transactions did we have?
2. What was the total amount sold?
3. How many different Customers did we have?
4. How many sales did we have in each region?
5. What was the amount sold by product?

---

**CRISP-DM**

This prompt and its tasks correspond to the **Data Understanding** stage of the CRISP-DM framework.

```mermaid
%%{init: {'theme': 'base', 'themeVariables': { 'primaryColor': '#c9dfed' }}}%%
stateDiagram 
direction LR
Business_Understanding --> Data_Understanding

state Data_Understanding {
direction LR
  Collect_Initial_Data
  Describe_Data
  Explore_Data
  Verify_Data_Quality
}

Data_Understanding --> Data_Prep
Data_Prep --> Modeling
Modeling --> Evaluation
Evaluation --> Deployment

```


<div class="page"/>


## Tasks

### Import the Packages/Libraries


**Import the Packages and Libraries into the File**

```python
import pandas as pd
import numpy as np
```

If other packages are needed during the analysis, it's best to import them at the top of the file with the other imports.

### Load the Data

**Load the sales_transactions.csv as a pandas DataFrame**

```python
df = pd.read_csv('sales_transactions.csv')
```

**Verify it loaded correctly**

```python
df.head()
```

This views the first 5 records in a dataframe, `.tail()` can be used for the last 5 records.

**Did it load correctly?**

It probably didn't look correct, it looks like the `.csv` is tab delimited. Try using the `sep='\t'` argument with the import again.

```python
df = pd.read_csv('sales_transactions.csv', sep='\t')
```

**Fix the `Amount` Column**

It appears the `Amount` column is being treated as a `str` instead of a `float`. The "$" needs to be removed and converted as a `float` type.

```python
df.loc[:,'Amount'] = df.loc[:,'Amount'].str.replace('$','').str.strip().astype(float)
```

<div class="page"/>


### Describe the Data

**Describing data one by one**

This can be a tedious task, and the next step will show us a better way to do it.

```python
df.shape
df['Product'].unique()
df['Amount'].sum()
df['Amount'].mean()
df['Amount'].std()
df['Amount'].max()
```

**Use the `.describe` method**

```python
df.describe()
```

If that doesn't look right, try it with the argument `include='all'`

```python
df.describe(include='all')
```


**Visualize the `Amount` column with a histogram**

Install the `matplotlib` package

```bash
pip install matplotlib
```

Import into the file

```python
import matplotlib
```

Use the `.hist()` method to create a histogram chart

```python
df['Amount'].hist()
```

Adjust the number of "bins"
```python
df['Amount'].hist(bins=30)
```


<div class="page"/>


### Describe the Data with Pandas Profiling

A better way than doing this manually is to use the `pandas-profiling` package.

Install pandas profiling and import it into the file.

```bash
pip install pandas-profiling
pip install ipywidgets
```

```python
from pandas_profiling import ProfileReport
```

**Create the profile object**

```python
profile = ProfileReport(df, title="Sales Profiling Report",explorative=True)
```

**Export the profile to a HTML file**

```python
profile.to_file('sales_report.html')
```


**Open the `sales_report.html` file**

<div class="page"/>

### Answer the Prompt Questions

How many sales transactions did we have?

```python
len(df['Transaction_Code'].unique())
```

What was the total amount sold?

```python
df['Amount'].sum()
```

How many different Customers did we have?

```python
len(df['Cust_ID'].unique())
```

How many sales did we have in each region?

```python
df[['Region','Amount']].groupby(['Region']).count()
```

What was the amount sold by product?

```python
df[['Product','Amount']].groupby(['Product']).sum()
```