# Data Analysis in Python

- [Data Analysis in Python](#data-analysis-in-python)
  - [Overview](#overview)
  - [1 Getting Started](#1-getting-started)
  - [Extra](#extra)
    - [data_transformation_process_mining](#data_transformation_process_mining)

## Overview

This repo contains some exercises/tutorials for learning **Data Analysis in Python**.

## 1 Getting Started

This tutorial focuses on getting a user up and running with Python and Jupyter Labs (/Notebooks) as fast as possible and how to do some basic data tasks in `pandas`.

## Extra

### data_transformation_process_mining

Reviews a problem and solution for transforming data for a processing mining project.